var Index = require('../routes/index');
var User = require('../routes/user');
var post = require('../routes/Post');
var up = require('../routes/upload');
module.exports = function (app) {

    // app 本身有很多方法，其中包括最常用的 get、post、put/patch、delete，
    // 在这里我们调用其中的 get 方法，为我们的 `/` 路径指定一个 handler 函数。
    // 这个 handler 函数会接收 req 和 res 两个对象，他们分别是请求的 request 和 response。
    // request 中包含了浏览器传来的各种信息，比如 query 啊，body 啊，headers 啊之类的，都可以通过 req 对象访问到。
    // res 对象，我们一般不从里面取信息，而是通过它来定制我们向浏览器输出的信息，比如 header 信息，比如想要向浏览器输出的内容
    
    //对用户的预处理
    app.use(function(req, res, next) {
        var _user, reg = new RegExp(/^\/admin.*/);
        //存储用户登陆信息 session
        _user = req.session.user;
        res.locals.user = _user;
        //判断是否登陆如果登陆直接跳转后台【管理界面】
        if (_user && req.url === '/login') return res.redirect('/admin');
        //针对所后台页面判断是否登陆，未登陆的跳转到登陆页面
        if (reg.test(req.url) && !_user) return res.redirect('/login');
        return next();
    });


    //后台页面 - 初始化数据
    app.get('/init', User.init)//初始化数据
       .post('/user_init', User.user_init);//POST提交-初始化数据

    //后台管理 - 页面登陆  
    app.get('/login', User.login) //post提交登录校验
       .post('/login', User.signin);

    //后台管理 - 登出页面/注销页面
    app.get('/logout', User.logout);
    app.get('/post/edit', post.edit)
    .get('/post/edit/:id', post.edit)//初始化数据
       .post('/post/post_edit', post.add);//POST提交

    app.get('/news/:id', post.detail).get("/news", post.detail);

    app.post("/upload", up.upload);
    //404页面  - 这个要放到最后面
    app.get('/*', Index.undefineds);

}
