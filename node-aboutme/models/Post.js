﻿var mongoose = require("mongoose");
var PostSchema = new mongoose.Schema({
   // id: mongoose.Schema.Types.ObjectId,
    title: String,//标题
    date: {
        createAt: {
            type: Date,
            default: Date.now()
        },
        updateAt: {
            type: Date,
            default: Date.now()
        }
    },
    pv: {
        type: Number,
        default: 0
    },//访问量
    user: String,
    comments: [],
    reprint_source: String,//转载来源
    contents: String,//新闻内容
    tags: [String]//新闻tags
});

PostSchema.pre('save', function (next) {
    if (this.isNew) {
        this.date.createAt = this.date.updateAt = Date.now();
    } else {
        this.date.updateAt = Date.now();
    }
    next();
});

PostSchema.statics = {
    fetch: function (cb) {
        return this.find({}).sort('date.updateAt').exec(cb);
    }
}
var Post = mongoose.model('Post', PostSchema);
module.exports = Post;